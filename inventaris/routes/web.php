<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/barang', 'BarangController@index');
Route::get('/barang/barangMasuk', 'BarangController@barangMasuk');
Route::post('/barang/store', 'BarangController@store');
Route::get('/barang/edit/{id}', 'BarangController@edit');
Route::put('/barang/update/{id}', 'BarangController@update');
Route::get('/barang/detail/{id}', 'BarangController@detail');
Route::get('/barang/hapus/{id}', 'BarangController@hapus');

Route::get('/user', 'PenggunaController@index');
Route::get('/user/tambah', 'PenggunaController@tambah');
Route::post('/user/store', 'PenggunaController@store');
Route::get('/user/edit/{id}', 'PenggunaController@edit');
Route::put('/user/update/{id}', 'PenggunaController@update');
Route::get('/user/delete/{id}', 'PenggunaController@delete');

Route::get('/transaksi', 'TransaksiController@index');
Route::get('/transaksi/tambah', 'TransaksiController@transaksi');
Route::post('/transaksi/store', 'TransaksiController@store');
