<?php
namespace App\DataTransferObject;

class TransaksiDTO extends GenericDTO
{
	private $idTransaksi;
	private $idPengguna;
	private $idBrg;
	private $jumlah;
	private $tglTransaksi;

	public function getIdTransaksi()
	{
		return $this->idTransaksi;
	}

	public function setIdTransaksi($idTransaksi):void
	{
		$this->idTransaksi = $idTransaksi;
	}

	public function getIdPengguna()
	{
		return $this->idPengguna;
	}

	public function setIdPengguna($idPengguna): void
	{
		$this->idPengguna = $idPengguna;
	}

	public function getIdBrg()
	{
		return $this->idBrg;
	}

	public function setIdBrg($idBrg): void
	{
		$this->idBrg = $idBrg;
	}

	public function getJumlah()
	{
		return $this->jumlah;
	}

	public function setJumlah($jumlah): void
	{
		$this->jumlah = $jumlah;
	}

	public function getTglTransaksi()
	{
		return $this->tglTransaksi;
	}

	public function setTglTransaksi($tglTransaksi): void
	{
		$this->tglTransaksi = $tglTransaksi;
	}
}