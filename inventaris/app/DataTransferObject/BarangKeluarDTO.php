<?php 
namespace App\DataTransferObject;

class BarangKeluarDTO extends GenericDTO
{
	private $idKeluar;
	private $idBrg;
	private $namaBrg;
	private $tglKeluar;
	private $jmlBrg;

	public function getIdKeluar()
	{
		return $this->idKeluar;
	}

	public function setIdKeluar($idKeluar): void
	{
		$this->idKeluar = $idKeluar;
	}

	public function getIdBrg()
	{
		return $this->idBrg;
	}

	public function setIdBrg($idBrg): void
	{
		$this->idBrg = $idBrg;
	}

	public function getNamaBrg()
	{
		return $this->namaBrg;
	}

	public function setNamaBrg($namaBrg): void
	{
		$this->namaBrg = $namaBrg;
	}

	public function getTglKeluar()
	{
		return $this->tglKeluar;
	}

	public function setTglKeluar($tglKeluar): void
	{
		$this->tglMasuk = $tglKeluar;
	}

	public function getJmlBrg()
	{
		return $this->jmlBrg;
	}

	public function setJmlBrg($jmlBrg): void
	{
		$this->jmlBrg = $jmlBrg;
	}
}