<?php 
namespace App\DataTransferObject;

class BarangDTO extends GenericDTO
{
	private $idBrg;
	private $namaBrg;
	private $tglMasuk;
	private $stokBrg;
	private $keterangan;

	public function getIdBrg()
	{
		return $this->idBrg;
	}

	public function setIdBrg($idBrg): void
	{
		$this->idBrg = $idBrg;
	}

	public function getNamaBrg()
	{
		return $this->namaBrg;
	}

	public function setNamaBrg($namaBrg): void
	{
		$this->namaBrg = $namaBrg;
	}

	public function getTglMasuk()
	{
		return $this->tglMasuk;
	}

	public function setTglMasuk($tglMasuk): void
	{
		$this->tglMasuk = $tglMasuk;
	}

	public function getStokBrg()
	{
		return $this->stokBrg;
	}

	public function setStokBrg($stokBrg): void
	{
		$this->stokBrg = $stokBrg;
	}

	public function getKeterangan()
	{
		return $this->keterangan;
	}

	public function setKeterangan($keterangan): void
	{
		$this->keterangan = $keterangan;
	}

}