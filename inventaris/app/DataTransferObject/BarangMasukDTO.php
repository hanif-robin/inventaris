<?php 
namespace App\DataTransferObject;

class BarangMasukDTO extends GenericDTO
{
	private $idMasuk;
	private $idBrg;
	private $namaBrg;
	private $tglMasuk;
	private $jmlBrg;

	public function getIdMasuk()
	{
		return $this->idMasuk;
	}

	public function setIdMasuk($idMasuk): void
	{
		$this->idMasuk = $idMasuk;
	}

	public function getIdBrg()
	{
		return $this->idBrg;
	}

	public function setIdBrg($idBrg): void
	{
		$this->idBrg = $idBrg;
	}

	public function getNamaBrg()
	{
		return $this->namaBrg;
	}

	public function setNamaBrg($namaBrg): void
	{
		$this->namaBrg = $namaBrg;
	}

	public function getTglMasuk()
	{
		return $this->tglMasuk;
	}

	public function setTglMasuk($tglMasuk): void
	{
		$this->tglMasuk = $tglMasuk;
	}

	public function getJmlBrg()
	{
		return $this->jmlBrg;
	}

	public function setJmlBrg($jmlBrg): void
	{
		$this->jmlBrg = $jmlBrg;
	}
}