<?php 
namespace App;
use Illuminate\Database\Eloquent\Model;

class Inventaris extends Model
{
	protected $table    = "barang";
	protected $primaryKey = "id_brg";
	protected $fillable  = ['nama_brg', 'tgl_masuk', 'stok_brg', 'keterangan'];
	public $timestamps  = FALSE;
}