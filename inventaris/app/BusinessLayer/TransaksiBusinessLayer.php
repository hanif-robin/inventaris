<?php 

namespace App\BusinessLayer;
use App\PersistanceLayer\TransaksiDAO;
use App\PresentationLayer\ResponseCreatorPresentationLayer;
use App\DataTransferObject\TransaksiDTO;

class TransaksiBusinessLayer extends GenericBusinessLayer
{
	public function aksiAmbilSemua()
	{
		try {
			$data = TransaksiDAO::all();
			if (count($data)==0) {
				$respose = new ResponseCreatorPresentationLayer(404,'Data Tidak Ditemukan', [],null);
				return $respose->getResponse();
			}
			$respose = new ResponseCreatorPresentationLayer(200,'Data Ditemukan', $data, null);
		} catch (\Exception $e) {
			$errors = $e->getMessage();
			$response = new ResponseCreatorPresentationLayer(500,'Server Sedang Perbaikan', [], $errors);
		}
		return $respose->getResponse();
	}

	public function aksiAmbilBerdasarId(TransaksiDTO $params)
	{
		try {
			$id = $params->getIdTransaksi();
			$data = TransaksiDAO::find($id);
			if (is_null($data)) {
				$response = new ResponseCreatorPresentationLayer(404,'Data Tidak Ditemukan', [],null);
				return $response->getResponse();
			}
			$response = new ResponseCreatorPresentationLayer(200,'Data Ditemukan', $data,null);
        } catch (\Exception $e) {
        	$errors = $e->getMassage();
			$response = new ResponseCreatorPresentationLayer(500,'Server Sedang Diperbaiki', [], $errors);
        }
        return $response->getResponse();
	}

	public function aksiTransaksi(TransaksiDTO $params)
	{
		try {
			$data = new TransaksiDAO();
			$data->id_pengguna = $params->getIdPengguna();
			$data->id_brg      = $params->getIdBrg();
			$data->jumlah      = $params->getJumlah();
			$data->save();

			if (is_null($data)) {
				$response = new ResponseCreatorPresentationLayer(404,'Data Tidak Ditemukan', [],null);
				return $response->getResponse();
			}
			$response = new ResponseCreatorPresentationLayer(200,'Data Ditemukan', $data,null);
		} catch (\Exception $e) {
			$errors = $e->getMessage();
			$response = new ResponseCreatorPresentationLayer(500,'Server Sedang Diperbaiki', [], $errors);
		}
		return $response->getResponse();
	}

	// public function aksiSimpanBerdasarId(TransaksiDTO $params)
	// {
	// 	$id = $params->getIdTransaksi();

	// 	$data = TransaksiDAO::find($id);
	// 	$data
	// }
}