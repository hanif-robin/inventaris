<?php
namespace App\BusinessLayer;
use App\PersistanceLayer\BarangDAO;
use App\PresentationLayer\ResponseCreatorPresentationLayer;
use App\DataTransferObject\BarangDTO;

class BarangBusinessLayer extends GenericBusinessLayer
{
	
	public function aksiAmbilSemua()
	{
		try {
			$data = BarangDAO::all();
			if (count($data) == 0) {
				$response = new ResponseCreatorPresentationLayer(404,'Data Tidak Ditemukan', [],null);
				return $response->getResponse();
			}

			$response = new ResponseCreatorPresentationLayer(200,'Data Ditemukan', $data,null);

		} catch (\Exeption $e) {
			$errors = $e->getMassage();
			$response = new ResponseCreatorPresentationLayer(500,'Server Sedang Diperbaiki', [], $errors);
		}

		return $response->getResponse();
	}

	public function aksiAmbilBerdasarId(BarangDTO $params)
    {
    	
        try {
        	$id = $params->getIdBrg();
        	$data = BarangDAO::find($id);
        	if (is_null($data)) {
				$response = new ResponseCreatorPresentationLayer(404,'Data Tidak Ditemukan', [],null);
				return $response->getResponse();
			}

			$response = new ResponseCreatorPresentationLayer(200,'Data Ditemukan', $data,null);
        } catch (\Exception $e) {
        	$errors = $e->getMassage();
			$response = new ResponseCreatorPresentationLayer(500,'Server Sedang Diperbaiki', [], $errors);
        }

        return $response->getResponse();
    }

    public function aksiSimpan(BarangDTO $params)
    {
  		try {
			$data             = new BarangDAO();
			$data->nama_brg    = $params->getNamaBrg();
			$data->stok_brg   = $params->getStokBrg();
			$data->keterangan = $params->getKeterangan();
			$data->save();

			if (is_null($data)) {
				$response = new ResponseCreatorPresentationLayer(404,'Data Tidak Ditemukan', [],null);
				return $response->getResponse();
			}

			$response = new ResponseCreatorPresentationLayer(200,'Data Ditemukan', $data,null);

		} catch (\Exeption $e) {
			$errors = $e->getMassage();
			$response = new ResponseCreatorPresentationLayer(500,'Server Sedang Diperbaiki', [], $errors);
		}

		return $response->getResponse();
    }
    public function aksiSimpanBerdasarId(BarangDTO $params)
	{
		try {
			$id = $params->getId();

			$data = BarangDAO::find($id);
			$data->nama_brg = $params->getNamaBrg();
			$data->keterangan = $params->getKeterangan();
			$data->save();

			if (is_null($data)) {
				$response = new ResponseCreatorPresentationLayer(404,'Data Tidak Ditemukan', [],null);
				return $response->getResponse();
			}

			$response = new ResponseCreatorPresentationLayer(200,'Data Ditemukan', $data,null);

		} catch (\Exeption $e) {
			$errors = $e->getMassage();
			$response = new ResponseCreatorPresentationLayer(500,'Server Sedang Diperbaiki', [], $errors);
		}

		return $response->getResponse();
	}

    public function aksiHapus(BarangDTO $params)
    {
        try {
        	$id = $params->getIdBrg();
        	$data = BarangDAO::find($id);
        	$data->delete();
        	if (is_null($data)) {
				$response = new ResponseCreatorPresentationLayer(404,'Data Tidak Ditemukan', [],null);
				return $response->getResponse();
			}

			$response = new ResponseCreatorPresentationLayer(200,'Data Ditemukan', $data,null);
        } catch (\Exception $e) {
        	$errors = $e->getMassage();
			$response = new ResponseCreatorPresentationLayer(500,'Server Sedang Diperbaiki', [], $errors);
        }
        return $response->getResponse();
    }
}
