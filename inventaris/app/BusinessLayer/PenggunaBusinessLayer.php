<?php 
namespace App\BusinessLayer;
use App\PersistanceLayer\PenggunaDAO;
use App\PresentationLayer\ResponseCreatorPresentationLayer;
use App\DataTransferObject\PenggunaDTO;

class PenggunaBusinessLayer extends GenericBusinessLayer
{
	public function aksiAmbilSemua()
	{
		try {
			$data = PenggunaDAO::all();
			if (count($data) == 0) {
				$response = new ResponseCreatorPresentationLayer(404,'Data Tidak Ditemukan', [],null);
				return $response->getResponse();
			}

			$response = new ResponseCreatorPresentationLayer(200,'Data Ditemukan', $data,null);

		} catch (\Exeption $e) {
			$errors = $e->getMassage();
			$response = new ResponseCreatorPresentationLayer(500,'Server Sedang Diperbaiki', [], $errors);
		}

		return $response->getResponse();
	}

	public function aksiAmbilBerdasarId(PenggunaDTO $params)
	{
		try {
        	$id = $params->getId();
        	$data = PenggunaDAO::find($id);
        	if (is_null($data)) {
				$response = new ResponseCreatorPresentationLayer(404,'Data Tidak Ditemukan', [],null);
				return $response->getResponse();
			}

			$response = new ResponseCreatorPresentationLayer(200,'Data Ditemukan', $data,null);
        } catch (\Exception $e) {
        	$errors = $e->getMassage();
			$response = new ResponseCreatorPresentationLayer(500,'Server Sedang Diperbaiki', [], $errors);
        }

        return $response->getResponse();
	}

	public function aksiSimpan(PenggunaDTO $params)
	{
		try {
			$data = new PenggunaDAO();
			$data->username = $params->getUsername();
			$data->password = $params->getPassword();
			$data->save();

			if (is_null($data)) {
				$response = new ResponseCreatorPresentationLayer(404,'Data Tidak Ditemukan', [],null);
				return $response->getResponse();
			}

			$response = new ResponseCreatorPresentationLayer(200,'Data Ditemukan', $data,null);

		} catch (\Exeption $e) {
			$errors = $e->getMassage();
			$response = new ResponseCreatorPresentationLayer(500,'Server Sedang Diperbaiki', [], $errors);
		}

		return $response->getResponse();
	}

	public function aksiSimpanBerdasarId(PenggunaDTO $params)
	{
		try {
			$id = $params->getId();

			$data = PenggunaDAO::find($id);
			$data->username = $params->getUsername();
			$data->password = $params->getPassword();
			$data->save();

			if (is_null($data)) {
				$response = new ResponseCreatorPresentationLayer(404,'Data Tidak Ditemukan', [],null);
				return $response->getResponse();
			}

			$response = new ResponseCreatorPresentationLayer(200,'Data Ditemukan', $data,null);

		} catch (\Exeption $e) {
			$errors = $e->getMassage();
			$response = new ResponseCreatorPresentationLayer(500,'Server Sedang Diperbaiki', [], $errors);
		}

		return $response->getResponse();
	}

	public function aksiHapus(PenggunaDTO $params)
    {
        try {
        	$id = $params->getId();
        	$data = PenggunaDAO::find($id);
        	$data->delete();
        	if (is_null($data)) {
				$response = new ResponseCreatorPresentationLayer(404,'Data Tidak Ditemukan', [],null);
				return $response->getResponse();
			}

			$response = new ResponseCreatorPresentationLayer(200,'Data Ditemukan', $data,null);
        } catch (\Exception $e) {
        	$errors = $e->getMassage();
			$response = new ResponseCreatorPresentationLayer(500,'Server Sedang Diperbaiki', [], $errors);
        }
        return $response->getResponse();
    }
}