<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\PersistanceLayer\TransaksiDao;
use App\DataTransferObject\TransaksiDTO;
use App\BusinessLayer\TransaksiBusinessLayer;

class TransaksiController extends Controller
{
	private $transaksiBusinessLayer;

	function __construct()
	{
		$this->transaksiBusinessLayer = new TransaksiBusinessLayer();
	}

	public function index()
	{
		$data = $this->transaksiBusinessLayer->aksiAmbilSemua();
		$params = [
			'title' => 'Transaksi',
			'data'  => $data['data']
		];
		return view('transaksi.transaksi', $params);
	}

	public function transaksi()
	{
		$params = [
			'title' => 'Tambah Transaksi'
		];
		return view('transaksi.transaksiTambah', $params);
	}

	public function store(Request $request)
	{
		$transaksiDTO = new TransaksiDTO();
		$transaksiDTO->setIdPengguna($request->input('idPengguna'));
		$transaksiDTO->setIdBrg($request->input('idBrg'));
		$transaksiDTO->setJumlah($request->input('jumlah'));

		$data = $this->transaksiBusinessLayer->aksiTransaksi($transaksiDTO);
		return redirect('/transaksi');
	}
}