<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\PersistanceLayer\BarangDao;
use App\DataTransferObject\BarangDTO;
use App\BusinessLayer\BarangBusinessLayer;

class BarangController extends Controller
{
    private $barangBusinessLayer;

    public function __construct()
    {
        $this->barangBusinessLayer = new BarangBusinessLayer();
    }
    public function index(Request $request)
    {
        $data = $this->barangBusinessLayer->aksiAmbilSemua();
        $params=[
            'title' => 'Daftar Barang',
            'data' => $data['data']
        ];
        return view('barang.barang', $params);
    }

    public function detail($id,Request $request)
    {
        $barangDTO = new BarangDTO();
        $barangDTO->setIdBrg($id);
        $data  = $this->barangBusinessLayer->aksiAmbilBerdasarId($barangDTO);
        $params=[
            'title' => 'Detail Barang',
            'data' => $data['data']
        ];
        return view('barang.barangDetail', $params);
    }

    public function edit($id)
    {   
        $barangDTO = new BarangDTO();
        $barangDTO->setIdBrg($id);
        $data  = $this->barangBusinessLayer->aksiAmbilBerdasarId($barangDTO);
        $params=[
            'title' => 'Edit Barang',
            'data' => $data['data']
        ];
        return view('barang.barangEdit', $params);
    }

    public function update($id, Request $request)
    {
        $barangDTO = new BarangDTO();
        $barangDTO->setIdBrg($id);
        $barangDTO->setNamaBrg($request->input('namaBrg'));
        $barangDTO->setKeterangan($request->input('keterangan'));
        
        $data = $this->barangBusinessLayer->aksiSimpanBerdasarId($barangDTO);

        return redirect('/barang');
    }
    
    public function hapus($id,Request $request)
    {
        $barangDTO = new BarangDTO();
        $barangDTO->setIdBrg($id);
        $data = $this->barangBusinessLayer->aksiHapus($barangDTO);
        
        return redirect('/barang');
    }
    
    public function barangMasuk()
    {
    	$params=[
    		'title' => 'Barang Masuk'
    	];
    	return view('barang.barangMasuk', $params);
    }
    public function store(Request $request)
    {
        $barangDTO = new BarangDTO();
        $barangDTO->setNamaBrg($request->input('namaBrg'));
        $barangDTO->setStokBrg($request->input('stokBrg'));
        $barangDTO->setKeterangan($request->input('keterangan'));
        
        $data = $this->barangBusinessLayer->aksiSimpan($barangDTO);
 
        return redirect('/barang');
    }

}
