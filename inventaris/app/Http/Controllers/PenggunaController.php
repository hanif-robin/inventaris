<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\PersistanceLayer\PenggunaDao;
use App\DataTransferObject\PenggunaDTO;
use App\BusinessLayer\PenggunaBusinessLayer;
class PenggunaController extends Controller
{
    private $barangBusinessLayer;

    public function __construct()
    {
        $this->penggunaBusinessLayer = new PenggunaBusinessLayer();
    }

    public function index()
    {
        $data = $this->penggunaBusinessLayer->aksiAmbilSemua();
        $params=[
            'title' => 'Pengguna',
            'data'  => $data['data']
            
        ];
        return view('pengguna.pengguna', $params);
    }
    public function tambah()
	{
		$params=[
            'title' => 'Tambah Pengguna'
        ];
		return view('pengguna.pengguna_tambah', $params);
	}

	public function store(Request $request)
	{
        $penggunaDTO = new PenggunaDTO();
        $penggunaDTO->setUsername($request->input('username'));
        $penggunaDTO->setPassword($request->input('password'));
 		
 		$data = $this->penggunaBusinessLayer->aksiSimpan($penggunaDTO);
        
    	return redirect('user');
	}

    public function edit($id, Request $request)
    {   
        $penggunaDTO = new PenggunaDTO();
        $penggunaDTO->setId($id);
        $data  = $this->penggunaBusinessLayer->aksiAmbilBerdasarId($penggunaDTO);
        $params=[
            'title' => 'Edit Pengguna',
            'data'  => $data['data']
        ];
        return view('pengguna.pengguna_edit', $params);
    }

    public function update($id, Request $request)
    {
 
        $penggunaDTO = new PenggunaDTO();
        $penggunaDTO->setId($id);
        $penggunaDTO->setUsername($request->input('username'));
        $penggunaDTO->setPassword($request->input('password'));
        
        $data = $this->penggunaBusinessLayer->aksiSimpanBerdasarId($penggunaDTO);

        return redirect('/user');
    }

    public function delete($id)
    {
        $penggunaDTO = new PenggunaDTO();
        $penggunaDTO->setId($id);
        $data = $this->penggunaBusinessLayer->aksiHapus($penggunaDTO);
        return redirect('/user');
    }
}
