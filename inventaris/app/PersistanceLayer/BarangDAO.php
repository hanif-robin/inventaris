<?php 
namespace App\PersistanceLayer;
use Illuminate\Database\Eloquent\Model;

class BarangDAO extends GenericDAO
{
	protected $table    = "barang";
	protected $primaryKey = "id_brg";
	protected $fillable = ['nama_brg','stok_brg','keterangan'];
	public $timestamps = FALSE;
}