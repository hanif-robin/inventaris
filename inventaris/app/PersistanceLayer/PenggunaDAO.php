<?php 
namespace App\PersistanceLayer;
use Illuminate\Database\Eloquent\Model;

class PenggunaDAO extends GenericDAO
{
	protected $table    = "pengguna";
	protected $primaryKey = "id";
	protected $fillable = ['username', 'password'];
	public $timestamps   = FALSE;
}