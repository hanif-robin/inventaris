<?php
namespace App\PersistanceLayer;
use Illuminate\Database\Eloquent\Model;

class TransaksiDAO extends GenericDAO
{
	protected $table = "transaksi";
	protected $primaryKey = "id_transaksi";
	protected $fillable = ['id_pengguna', 'id_brg', 'jumlah'];
	public $timestamps = FALSE;

	public function getPengguna()
	{
		return $this->hasone('App\PersistanceLayer\PenggunaDAO','id','id_pengguna');
	}

	public function getBarang()
	{
		return $this->hasone('App\PersistanceLayer\BarangDAO', 'id_brg', 'id_brg');
	}
}