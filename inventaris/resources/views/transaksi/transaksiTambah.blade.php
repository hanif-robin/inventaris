@extends('layouts.main')
@section('title', $title)
@section('content')
	<div class="container mt-3">
	  <h3>Tambah Transaksi</h3>
      <form method="post" action="{{url('transaksi/store')}}">

        {{csrf_field()}}

        <div class="form-group">
          <label>ID Pengguna</label>
          <input type="text" name="idPengguna" class="form-control" aria-describedby="emailHelp" placeholder="Masukkan ID Pengguna">
        </div>
        <div class="form-group">
          <label>ID Barang</label>
          <input type="text" name="idBrg" class="form-control" aria-describedby="emailHelp" placeholder="Masukkan ID Barang">
        </div>
        <div class="form-group">
          <label>Jumlah</label>
          <input type="text" name="jumlah" class="form-control" placeholder="Masukkan Jumlah">
        </div>
        <div class="row">
          <div class="col-1"><button type="submit" class="btn btn-primary">Submit</button></div>
          <div class="col"><a href="{{url('/transaksi')}}" class="btn btn-danger">Kembali</a></div>
        </div>
      </form>

    </div>
@endsection