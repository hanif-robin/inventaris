@extends('layouts.main')
@section('title', $title)
@section('content')
    <div class="container mt-3">
      <h3>Daftar Barang</h3>
      <hr class="sidebar-divider my-0">
      <div class=" mt-3 mb-3"><a class="btn btn-primary" href="transaksi/tambah">Tambah Transaksi</a></div>
      <table class="table table-hover table-bordered text-center">
        <thead class="thead-dark">
          <tr>
              <th scope="col">Id Transaksi</th>
              <th scope="col">Nama Pengguna</th>
              <th scope="col">Alamat</th>
              <th scope="col">Nama Barang</th>
              <th scope="col">Jumlah</th>
              <th scope="col">Tanggal Transaksi</th>
              <th scope="col">OPSI</th>
          </tr>
        </thead>
        <tbody>
          @foreach($data as $t)
          <tr>
            <td>{{$t->id_transaksi}}</td>
            <td>{{$t->getPengguna->username}}</td>
            <td>{{$t->getPengguna->password}}</td>
            <td>{{$t->getBarang->nama_brg}}</td>
            <td>{{$t->jumlah}}</td>
            <td>{{date('d/m/Y H.i', strtotime($t->tgl_transaksi))}}</td>
            <td><a href="transkasi/hapus/{{ $t->id_transaksi }}" class="btn btn-danger">Hapus</a>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
@endsection