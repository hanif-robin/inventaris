@extends('layouts.main')
@section('title', $title)
@section('content')
	<div class="container mt-3">
      <h3>Daftar Pengguna</h3>
      <hr class="sidebar-divider my-0">
      <table class="table table-hover table-bordered text-center">
        <thead class="thead-dark">
          <tr>
              <th scope="">ID Pengguna</th>
              <th scope="col">Nama Lengkap</th>
              <th scope="col">Alamat</th>
              <th scope="col">OPSI</th>
          </tr>
        </thead>
        <tbody>
          @foreach($data as $user)
          <tr>
            <td>{{$user->id}}</td>
            <td>{{$user->username}}</td>
            <td>{{$user->password}}</td>
            <td><a href="user/edit/{{ $user->id }}" class="btn btn-warning">Edit</a>
                <a href="user/delete/{{ $user->id }}" class="btn btn-danger">Hapus</a>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
      <div><a class="btn btn-primary" href="user/tambah">Tambah Data</a></div>
    </div>
@endsection