@extends('layouts.main')
@section('title', $title)
@section('content')

	<div class="container">
      <form method="post" action="{{url('/user/update')}}/{{ $data->id }}">

        {{ csrf_field() }}
        {{ method_field('PUT') }}
        <div class="form-group">
            <label>Nama Lengkap</label>
            <input type="text" name="username" class="form-control" value=" {{ $data->username }}">
        </div>
        <div class="form-group">
          <label>Alamat</label>
          <input type="text" name="password" class="form-control" value=" {{ $data->password }}">
        </div>
        <div class="form-group">
          <input type="submit" class="btn btn-success" value="Simpan">
        </div>
        <div class="row">
          <div class="col-1"><button type="submit" class="btn btn-primary">Submit</button></div>
          <div class="col"><a href="{{url('/user')}}" class="btn btn-danger">Kembali</a></div>
        </div>
      </form>

    </div>

@endsection