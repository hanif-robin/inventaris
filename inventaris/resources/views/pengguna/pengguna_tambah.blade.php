@extends('layouts.main')
@section('title', $title)
@section('content')
	<div class="container mt-3">
	  <h3>Tambah Pengguna Baru</h3>
      <form method="post" action="{{url('user/store')}}">

        {{csrf_field()}}

        <div class="form-group">
          <label for="exampleInputEmail1">Nama Lengkap</label>
          <input type="text" name="username" class="form-control" aria-describedby="emailHelp" >
        </div>
        <div class="form-group">
          <label for="exampleInputPassword1">Alamat</label>
          <input type="text" name="password" class="form-control">
        </div>
        <div class="row">
          <div class="col-1"><button type="submit" class="btn btn-primary">Submit</button></div>
          <div class="col"><a href="{{url('/user')}}" class="btn btn-danger">Kembali</a></div>
        </div>
      </form>

    </div>
@endsection