<!DOCTYPE html>
<html>
<head>
	<title>@yield('title')</title>
	@include('layouts.header')
</head>
<body>

	@include('layouts.sidebar')
	@include('layouts.body')

</body>
</html>