<script src="{{asset('public/assets/vendor/jquery/jquery.min.js')}"></script>
<script src="{{asset('public/assets/vendor/bootstrap/js/bootstrap.bundle.min.js')}"></script>
<script src="{{asset('public/assets/vendor/jquery-easing/jquery.easing.min.js')}"></script>
<script src="{{asset('public/assets/js/sb-admin-2.min.js')}"></script>
<script src="{{asset('public/assets/vendor/chart.js/Chart.min.js')}"></script>
<script src="{{asset('public/assets/js/demo/chart-area-demo.js')}"></script>
<script src="{{asset('public/assets/js/demo/chart-pie-demo.js')}"></script>
