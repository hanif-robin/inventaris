  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="sidebar-brand-icon">
          <i class="fas fa-file-invoice"></i>
        </div>
        <div class="sidebar-brand-text mx-3">Inventaris</div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item">
        <a class="nav-link" href="dashboard">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="user">
          <i class="fas fa-fw fa-user"></i>
          <span>Pengguna</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="barang">
          <i class="fas fa-fw fa-copy"></i>
          <span>Barang</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="transaksi">
          <i class="fas fa-fw fa-money-bill"></i>
          <span>Transaksi</span></a>
      </li>

    </ul>