  <link href="{{asset('public/assets/vendor/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
  <link href="{{asset('public/assets/css/sb-admin-2.min.css')}}" rel="stylesheet">
  <script src="{{asset('public/assets/vendor/jquery/jquery.min.js')}"></script>
<script src="{{asset('public/assets/vendor/bootstrap/js/bootstrap.bundle.min.js')}"></script>
<script src="{{asset('public/assets/vendor/jquery-easing/jquery.easing.min.js')}"></script>
<script src="{{asset('public/assets/js/sb-admin-2.min.js')}"></script>
<script src="{{asset('public/assets/vendor/chart.js/Chart.min.js')}"></script>
<script src="{{asset('public/assets/js/demo/chart-area-demo.js')}"></script>
<script src="{{asset('public/assets/js/demo/chart-pie-demo.js')}"></script>