@extends('layouts.main')
@section('title', $title)
@section('content')
	<div class="container mt-3">
	  <h3>Barang Masuk</h3>
      <form method="post" action="{{url('barang/store')}}">

        {{csrf_field()}}

        <div class="form-group">
          <label>Nama Barang</label>
          <input type="text" name="namaBrg" class="form-control" placeholder="Masukkan Nama Barang">
        </div>
        <div class="form-group">
          <label>Jumlah Barang</label>
          <input type="text" name="stokBrg" class="form-control" placeholder="Masukkan Jumlah">
        </div>
        <div class="form-group">
          <label>Keterangan Barang</label>
          <input type="text" name="keterangan" class="form-control" placeholder="Masukkan Keterangan">
        </div>
        <div class="row">
          <div class="col-1"><button type="submit" class="btn btn-primary">Submit</button></div>
          <div class="col"><a href="{{url('/barang')}}" class="btn btn-danger">Kembali</a></div>
        </div>
      </form>
    </div>
@endsection