@extends('layouts.main')
@section('title', $title)
@section('content')

	<div class="container">
      <form method="post" action="{{url('/barang/update')}}/{{ $data->id }}">

        {{ csrf_field() }}
        {{ method_field('PUT') }}
        <div class="form-group">
          <label>Nama Barang</label>
          <input type="text" name="namaBrg" class="form-control" value=" {{ $data->nama_brg }}">
        </div>
        <div class="form-group">
          <label>Keterangan</label>
          <input type="text" name="keterangan" class="form-control" value=" {{ $data->keterangan }}">
        </div>
        <div class="form-group">
        </div>
        <div class="row">
          <div class="col-1"><button type="submit" class="btn btn-primary">Submit</button></div>
          <div class="col"><a href="{{url('/barang')}}" class="btn btn-danger">Kembali</a></div>
        </div>
      </form>

    </div>

@endsection