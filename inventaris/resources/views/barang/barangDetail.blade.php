@extends('layouts.main')
@section('title', $title)
@section('content')
    <div class="container mt-3">
      <h3>Detail Barang</h3>
      <hr class="sidebar-divider my-0">
      <table class="table table-hover table-bordered text-center">
        <thead class="thead-dark">
          <tr>
              <th scope="col">Nama Barang</th>
              <th scope="col">Stok</th>
              <th scope="col">Tanggal Masuk</th>
              <th scope="col">Tanggal Keluar</th>
              <th scope="col">Keterangan</th>
          </tr>
        </thead>
        <tbody>

          <tr>
            <td>{{$data->nama_brg}}</td>
            <td>{{$data->stok_brg}}</td>
            <td>{{$data->tgl_masuk}}</td>
            <td>{{$data->tgl_keluar}}</td>
            <td>{{$data->keterangan}}</td>
          </tr>
        </tbody>
      </table>
      <a href="{{url('/barang')}}" class="btn btn-primary">Kembali</a>
    </div>
@endsection