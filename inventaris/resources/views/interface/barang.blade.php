@extends('layouts.main')
@section('title', $title)
@section('content')
    <div class="container mt-3">
      <h3>Daftar Barang</h3>
      <hr class="sidebar-divider my-0">
      <table class="table table-hover table-bordered text-center">
        <thead class="thead-dark">
          <tr>
              <th scope="col">Nama Barang</th>
              <th scope="col">Stok</th>
              <th scope="col">Keterangan</th>
              <th scope="col">OPSI</th>
          </tr>
        </thead>
        <tbody>
          @foreach($barang as $brg)
          <tr>
            <td>{{$brg->nama_brg}}</td>
            <td>{{$brg->stok_brg}}</td>
            <td>{{$brg->keterangan}}</td>
            <td><a href="barang/edit/{{ $brg->id_brg }}" class="btn btn-warning">Edit</a>
                <a href="barang/hapus/{{ $brg->id_brg }}" class="btn btn-danger">Hapus</a>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
      <div><a class="btn btn-primary" href="barang/tambah">Tambah Data</a></div>
    </div>
@endsection