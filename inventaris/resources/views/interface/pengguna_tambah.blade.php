@extends('layouts.main')
@section('title', $title)
@section('content')
	<div class="container mt-3">
	  <h3>Tambah Pengguna Baru</h3>
      <form method="post" action="{{url('user/store')}}">

        {{csrf_field()}}

        <div class="form-group">
          <label for="exampleInputEmail1">username</label>
          <input type="text" name="username" class="form-control" aria-describedby="emailHelp" placeholder="Masukkan username">
        </div>
        <div class="form-group">
          <label for="exampleInputPassword1">Password</label>
          <input type="password" name="password" class="form-control" placeholder="Masukkan Password">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
      </form>
    </div>
@endsection